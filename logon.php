<!DOCTYPE HTML>  
<html>
<head>
<style>
.error {color: #FF0000;}
form {border: 3px solid #17e62f;}

input[type=text], input[type=password] {
  width: 100%;
  padding: 12px 20px;
  margin: 8px 0;
  display: inline-block;
  border: 1px solid #ccc;
  box-sizing: border-box;
}

button {
  background-color: #4CAF50;
  color: white;
  padding: 14px 20px;
  margin: 8px 0;
  border: none;
  cursor: pointer;
  width: 100%;
}

button:hover {
  opacity: 0.8;
}

.container {
  padding: 16px;
}

</style>
</head>
<body>  

<?php
// define variables and set to empty values
$uname = $pwd = "";
$unameErr = $pwdErr = "";

if (isset($_POST['submit'])) {
  if (empty($_POST['pwd'])) {
    $unameErr = "Username is required";
  } else {
    $uname = test_input($_POST['uname']);
  }

  if (empty($_POST['pwd'])) {
    $pwdErr = "Password is required";
  } else {
    $pwd = test_input($_POST['pwd']);
  }
}

function test_input($data) {
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}
?>

<h2>PHP Form Validation Example</h2>
<p><span class="error">* required field</span></p>
<form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>"> 
  <div class="container">
    Username: <input type="text" name="uname">
    <span class="error">* <?php echo $unameErr;?></span>
    <br><br>
    Password: <input type="text" name="pwd">
    <span class="error">* <?php echo $pwdErr;?></span>
    <br><br>
    <button type="submit" name="submit">Log On</button> 
  </div> 
</form>

<?php
echo "<h2>Your Input:</h2>";
echo "<br>";

if (isset($_POST['submit'])) {
  if($pwd=='vcn198?' && $uname=='Tom') {
    echo $uname;
    echo "<br>";
    echo $pwd;
  } else {
    $unameErr = "Username is invalid";
    $pwdErr = "Password is invalid";
  }

  if($pwd=='vcn198?' && $uname=='Patrick') {
    echo $uname;
    echo "<br>";
    echo $pwd;
  } else {
    $unameErr = "Username is invalid";
    $pwdErr = "Password is invalid";
  }
}
?> 

</body>
</html>
